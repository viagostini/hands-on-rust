use crate::prelude::*;

#[system]
#[read_component(Point)]
#[read_component(Name)]
#[read_component(Health)]
pub fn tooltips(ecs: &SubWorld, #[resource] mouse_pos: &Point, #[resource] camera: &Camera) {
    let mut positions = <(Entity, &Point, &Name)>::query();

    let offset = Point::new(camera.left_x, camera.top_y);
    let map_pos = *mouse_pos + offset;
    let mut draw_batch = DrawBatch::new();

    draw_batch.target(HUD_TERMINAL);

    positions
        .iter(ecs)
        .filter(|(_, pos, _)| **pos == map_pos)
        .for_each(|(entity, _, name)| {
            let screen_pos = Point::new(
                mouse_pos.x * HUD_DISPLAY_SCALING_FACTOR.pow(2),
                mouse_pos.y * HUD_DISPLAY_SCALING_FACTOR.pow(2) - 2,
            );
            let display =
                if let Ok(health) = ecs.entry_ref(*entity).unwrap().get_component::<Health>() {
                    format!("{} : {} hp", &name.0, health.current)
                } else {
                    name.0.clone()
                };
            draw_batch.print_centered_at(screen_pos, &display);
        });

    draw_batch.submit(10100).expect("Batch error");
}
